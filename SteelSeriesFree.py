import evdev

class SteelSeriesFree:

	#Checks currently available devices, and creates an evdev.InputDevice instance for the physical device address specified.
	def __init__(self, physicalAddress):
		devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
		for device in devices:
			print(device.path, device.name, device.phys)
			if(device.phys == physicalAddress):
				self.device = evdev.InputDevice(device.path)
				print(self.device)

	#Events coming from evdev have slightly different makeup than the ones from the inputs module
	#Since the inputs module was the one I started with, this method converts evdev events into a format the Existing code can interpret
	#Since we only care about axis/button events, only those two are handled- the rest is ignored.
	def convertToCustomEvent(self, event):
		inputEvent = evdev.categorize(event)
		if(type(inputEvent) == evdev.events.KeyEvent):
			print(inputEvent.keycode, inputEvent.keystate)
			return Event(inputEvent.keycode, inputEvent.keystate)
		elif(type(inputEvent)== evdev.events.AbsEvent):
			print(evdev.ecodes.ABS[event.code], event.value)
			return Event(evdev.ecodes.ABS[event.code], event.value)
		else:
			return Event('','')

class Event:
	def __init__(self, code, state):
		self.code = code
		self.state = state
