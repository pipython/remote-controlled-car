from MotorDriver import MotorDriver
from inputs import get_gamepad

LEFT_TRIGGER_DEADZONE = 380
RIGHT_TRIGGER_DEADZONE = 380

class EventHandler():
    id = None
    def __init__(self, carController):
        self.carController= carController
        carController.loadEventHandler(self)

    def shouldHandle(self, event):
        return event.code == self.id

    def execute(self, event):
        print(event.ev_type, event.code, event.state)

    def handle(self,event):
        if(self.shouldHandle(event)):
            self.execute(event)        
    

class RightTrigger(EventHandler):
    MAX_SPEED = 75
    REST_VAL = 300
    id = 'ABS_RZ'

    def execute(self, event):
        if(event.state < self.REST_VAL):
            print('Stopping motor 0')
            self.carController.motorDriver.S(0)
        else:
            speed = min(self.MAX_SPEED, (event.state) / 10)
            print('Setting Motor 0 Speed to ', speed)
            self.carController.motorDriver.F(0, speed)

class LeftTrigger(EventHandler):
    MAX_SPEED = 75
    REST_VAL = 300
    id = 'ABS_Z'

    def execute(self, event):
        if(event.state < self.REST_VAL):
            print('Stopping motor 0')
            self.carController.motorDriver.S(0)
        else:
            speed = min(self.MAX_SPEED, (event.state) / 10)
            print('Setting Motor 0 Speed to ', speed)
            self.carController.motorDriver.B(0, speed)

class AKey(EventHandler):
    id = 'BTN_SOUTH'
    def execute(self, event):
        if event.state == 1:
            print('Stopping motor 0')
            self.carController.motorDriver.S(0)
        
class BKey(EventHandler):
    id = 'BTN_EAST'
    
class YKey(EventHandler):
    id = 'BTN_WEST'
    
class XKey(EventHandler):
    id = 'BTN_NORTH'
    
class RShoulder(EventHandler):
    id = 'BTN_TR'

class LShoulder(EventHandler):
    id = 'BTN_TL'

class Start(EventHandler):
    id = 'BTN_START'

class Select(EventHandler):
    id = 'BTN_SELECT'

class Xbox(EventHandler):
    id = 'BTN_MODE'

class RStickButton(EventHandler):
    id = 'BTN_THUMBR'

class LStickButton(EventHandler):
    id = 'BTN_THUMBL'

class CarController():

    def __init__(self):
        self.eventHandlers = []
        self.motorDriver = MotorDriver()
    
    def loadEventHandler(self,eventHandler):
        self.eventHandlers.append(eventHandler)
    
    def run(self):       
        while 1:
            events = get_gamepad()
            for event in events:
                for eventHandler in self.eventHandlers:
                    eventHandler.handle(event)


def main():
    carController = CarController()
    RightTrigger(carController)
    #LeftTrigger(carController)
    AKey(carController)
    BKey(carController)
    XKey(carController)
    YKey(carController)
    RShoulder(carController)
    LShoulder(carController)
    Start(carController)
    Select(carController)
    Xbox(carController)
    RStickButton(carController)
    LStickButton(carController)
    carController.run()

if __name__ == "__main__":
    main()


