from MotorDriver import MotorDriver
from SteelSeriesFree import SteelSeriesFree

# Each button on the gamepad needs to have a corresponding child class of the Event Handler 
# which will specify what should happen when the button is acted upon.
class EventHandler():
    id = None
    def __init__(self, carController):
        self.carController= carController
        carController.loadEventHandler(self)

    #Checks if the id of the Handler is contained within Event Codes coming from the gamepad.
    def shouldHandle(self, event):
        return self.id in event.code

    #Main logic for the Event Handler.
    def execute(self, event):
        print(event.code, event.state)

    def handle(self,event):
        if(self.shouldHandle(event)):
            self.execute(event)        
    
# Left Stick on SteelSeriesFree is used for throttle (forward/backwards)
class LeftStick(EventHandler):
    MAX_SPEED = 100
    REST_VAL = [5,-5]
    MAX_AXIS = 128
    AXIS_DIFFERENCE = MAX_AXIS-MAX_SPEED 
    id = 'ABS_Y'

    def execute(self, event):
        if(event.state in self.REST_VAL):
            print('Stopping motor 0')
            self.carController.motorDriver.S(0)
        else:
            if(event.state > 0):
                speed = min(self.MAX_SPEED, (event.state)-self.AXIS_DIFFERENCE)
                print('Motor 0 Going backwards at: ', speed)
                self.carController.motorDriver.B(0, speed)
            else:
                speed = min(self.MAX_SPEED, (-event.state)-self.AXIS_DIFFERENCE)
                print('Motor 0 Going forwards at: ', speed)
                self.carController.motorDriver.F(0, speed)

# AKey (key 4) used for stopping the motor.
class AKey(EventHandler):
    id = 'BTN_SOUTH'
    def execute(self, event):
        if event.state == 1:
            print('Stopping motor 0')
            self.carController.motorDriver.S(0)
        
class BKey(EventHandler):
    id = 'BTN_EAST'
    
class YKey(EventHandler):
    id = 'BTN_WEST'
    
class XKey(EventHandler):
    id = 'BTN_NORTH'
    
class RShoulder(EventHandler):
    id = 'BTN_TR'

class LShoulder(EventHandler):
    id = 'BTN_TL'

class Start(EventHandler):
    id = 'BTN_START'

class Select(EventHandler):
    id = 'BTN_SELECT'

class Xbox(EventHandler):
    id = 'BTN_MODE'

class RStickButton(EventHandler):
    id = 'BTN_THUMBR'

class LStickButton(EventHandler):
    id = 'BTN_THUMBL'

class CarController():

    def __init__(self, controllerAddress):
        self.eventHandlers = []
        self.motorDriver = MotorDriver()
        self.gamepad = SteelSeriesFree(controllerAddress)
    
    def loadEventHandler(self,eventHandler):
        self.eventHandlers.append(eventHandler)
    
    #Loop through events coming from the device defined in the SteelSeriesFree class.
    #For each even convert it to an event the handlers can interpret.
    #This is done to ensure minimal changes necessary between BT and USB modules.
    # TODO: join USB and BT code together.
    def run(self):       
        for event in self.gamepad.device.read_loop():
            customEvent = self.gamepad.convertToCustomEvent(event)
            for eventHandler in self.eventHandlers:
                eventHandler.handle(customEvent)


def main():
    carController = CarController('b8:27:eb:0a:ee:40')
    LeftStick(carController)
    AKey(carController)
    BKey(carController)
    XKey(carController)
    YKey(carController)
    RShoulder(carController)
    LShoulder(carController)
    Start(carController)
    Select(carController)
    Xbox(carController)
    RStickButton(carController)
    LStickButton(carController)
    carController.run()

if __name__ == "__main__":
    main()


