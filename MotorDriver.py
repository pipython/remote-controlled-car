
#!/usr/bin/python

from PCA9685 import PCA9685
import time

Dir = [
    'F',
    'B',
]

class MotorDriver():
    def __init__(self, address = 0x40, debug = False, pwmFreq=50):
        self.pwm = PCA9685(address, debug)
        self.PWMA = 0
        self.AIN1 = 1
        self.AIN2 = 2
        self.PWMB = 5
        self.BIN1 = 3
        self.BIN2 = 4
        self.pwm.setPWMFreq(pwmFreq)

    def Run(self, motor, index, speed):
        if speed > 100:
            return
        if(motor == 0):
            self.pwm.setDutycycle(self.PWMA, speed)
            if(index == Dir[0]):
                print ("1")
                self.pwm.setLevel(self.AIN1, 0)
                self.pwm.setLevel(self.AIN2, 1)
            else:
                print ("2")
                self.pwm.setLevel(self.AIN1, 1)
                self.pwm.setLevel(self.AIN2, 0)
        else:
            self.pwm.setDutycycle(self.PWMB, speed)
            if(index == Dir[0]):
                print ("3")
                self.pwm.setLevel(self.BIN1, 0)
                self.pwm.setLevel(self.BIN2, 1)
            else:
                print ("4")
                self.pwm.setLevel(self.BIN1, 1)
                self.pwm.setLevel(self.BIN2, 0)

    def Stop(self, motor):
        if (motor == 0):
            self.pwm.setDutycycle(self.PWMA, 0)
        else:
            self.pwm.setDutycycle(self.PWMB, 0)

    def F(self, motor, speed):
        if speed > 100:
            return
        self.Run(motor, 'F', speed)

    def B(self, motor, speed):
        if speed > 100:
            return
        self.Run(motor, 'B', speed)

    def S(self, motor):
        self.Stop(motor)
